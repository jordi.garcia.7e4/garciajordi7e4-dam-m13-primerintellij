## Barcelona

----

Amb una població d'1 602 386 habitants (2014), Barcelona és la ciutat més poblada de Catalunya i l'onzena de la Unió Europea, segona ciutat no capital d'estat després d'Hamburg. És el principal nucli urbà de la regió metropolitana de Barcelona, que aglutina 4 774 561 habitants, i de l'àrea Metropolitana de Barcelona, integrada per 36 municipis, que té una població de 3 225 058 habitants i una superfície de 633 km². Un estudi del departament d'Afers Socials i Econòmics de l'ONU de l'any 2005 situa Barcelona en la posició 51 del rànquing de les poblacions amb major nombre d'habitants del món.
### Llista ordenada de barris

1. Noubarris
2. Guinardo
3. Trinitat

### Llista desordenada de barris

- Trinitat Nova
- Guineuta
- Ciutat Vella

**Enllaç a una web [enllaç a Barcelona](https://ca.wikipedia.org/wiki/Barcelona)**

### BARCELONA

----

![Imatge de Barcelona](img/Barcelona.jpg)